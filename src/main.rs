/*
This code is multi layer parallel perceptron in rust
 */
extern crate ndarray;

use ndarray::Array1;
use ndarray_rand::rand_distr::Uniform;
use ndarray_rand::RandomExt;

pub trait Layer {
    /*fn new_(units: usize) -> Self;*/
    fn build(&mut self, input_size: usize);
    fn call(&mut self, input: &Array1<f32>) -> &Array1<f32>;
    fn print(&self) {
        println!("Layer");
    }
    fn get_output_size(&self) -> usize;
}

struct Dense {
    size: usize,
    weight: Array1<f32>,
    placeholder: Array1<f32>,
}
impl Layer for Dense {
    // fn new_(units: usize) -> Self {
    //     Dense {
    //         size: units,
    //         weight: Default::default(),
    //         placeholder: Default::default(),
    //     }
    // }

    fn build(&mut self, input_size: usize) {
        self.weight = Array1::random(input_size * self.size, Uniform::new(-1.0, 1.0));
        self.placeholder = Array1::zeros(self.size);
    }

    fn call(&mut self, input: &Array1<f32>) -> &Array1<f32> {
        for i in 0..self.size {
            let mut sum = 0.0;
            for j in 0..input.len() {
                sum += input[j] * self.weight[i * input.len() + j];
            }
            self.placeholder[i] = sum;
        }
        &self.placeholder
    }

    fn get_output_size(&self) -> usize {
        self.size
    }
}

struct Sequential {
    layers: Vec<Box<dyn Layer>>,
}

impl Sequential {
    fn new() -> Self {
        Self {
            layers: Default::default(),
        }
    }

    fn add(&mut self, layer: Box<dyn Layer>) {
        self.layers.push(layer);
    }

    fn build(&mut self, input_size: usize) {
        let mut input_size = input_size;
        for layer in self.layers.iter_mut() {
            layer.build(input_size);
            input_size = layer.get_output_size();
        }
    }

    fn call(&mut self, input: &Array1<f32>) -> Array1<f32> {
        let mut output = input;
        for layer in self.layers.iter_mut() {
            output = layer.call(output);
        }
        output.clone()
    }
}

fn main() {
    let input = Array1::from(vec![1.0, 2.0, 3.0, 4.0]);
    println!("{}", input);

    let layer = Dense {
        size: 64,
        weight: Default::default(),
        placeholder: Default::default(),
    };
    //layer.build(4);

    let layer2 = Dense {
        size: 32,
        weight: Default::default(),
        placeholder: Default::default(),
    };
    //layer2.build(3);

    let layer3 = Dense {
        size: 16,
        weight: Default::default(),
        placeholder: Default::default(),
    };
    let layer4 = Dense {
        size: 8,
        weight: Default::default(),
        placeholder: Default::default(),
    };

    //layer3.build(2);

    let mut model = Sequential::new();
    model.add(Box::new(layer));
    model.add(Box::new(layer2));
    model.add(Box::new(layer3));
    model.add(Box::new(layer4));

    model.build(4);

    let output = model.call(&input);

    println!("{}", output);
}
